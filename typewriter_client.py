# coding:utf-8
#!/usr/bin/env python
from __future__ import print_function
import socket
from contextlib import closing
import xml.etree.ElementTree as ET
import sys
from signal import signal, SIGPIPE, SIG_DFL, SIGINT, SIGTERM
import time
import re

#Ctrl+C de anzen ni teisi
def stop_handler(signum, frame):
    print("\nget signal ctrl+C")
    sock.close()
    sock2.close()
    time.sleep(5)
    sys.exit()

def msg_split(data):
    cmd = []
    try:
        data = ""
        SCORE = 0
        AMSCORE = 0
        while 1:
            if(data.find("<INPUT STATUS=\"LISTEN\"")):
                data = re.sub(r"<INPUT STATUS=\"LISTEN\"[\w/:%#\$&\?\(\)~\.=\+\-…]+", "", data)
            data = data.replace("\n.","")
            if "</RECOGOUT>" in data:
                sys.stderr.write(data)
                f = open("typewriter.log","w")
                f.write('<?xml version="1.0" encoding="UTF-8"?>\n' + data[data.find("<RECOGOUT>"):])
                f.close()
                root = ET.fromstring('<?xml version="1.0" encoding="UTF-8"?>\n' + data[data.find("<RECOGOUT>"):].replace("</s>","&lt;/s&gt;").replace("<s>","&lt;s&gt;").replace("\n.", "").replace(".",""))
                for score in root.findall("./SHYPO"):
                    SCORE = float(score.get("SCORE"))
                #    AMSCORE =("acoustic=" + score.get("AMSCORE"))
                for whypo in root.findall("./SHYPO/WHYPO"):
                    if(whypo.get("WORD") != "silB" and whypo.get("WORD") != "silE"):
                        cmd.append(whypo.get("PHONE").replace(" ","/"))
                        #print(whypo.get("PHONE"))
                data = ""
                break
            else:
                data = data + sock.recv(1024)
    except KeyboardInterrupt:
        sock.close()
        sys.exit()

    SCORE = ACOUSTIC = SCORE / 2.0
    cmd = "/".join(cmd)
    cmd = "comment=START-OF-FILE\n\nUTTERANCE=1\n\nNBEST=1\n\nORDER=1 WORDS=UTT-START/" + cmd + "/UTT-END/ score=" + str(SCORE) + " acoustic=" + str(ACOUSTIC) + "\n\ncomment=END-OF-FILE"
    conn.send(cmd)
    print(cmd)

def main():
    host = "localhost"
    port = 10501
    port2 = 6003
    backlog = 10
    bufsize = 4096
    global sock
    global sock2
    global conn
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host,port))
    sock2 = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

    sock2.bind((host,port2))
    sock2.listen(backlog)

    conn,address = sock2.accept()
    while(1):
        recv_data = sock.recv(bufsize)

        #sys.stderr.write(recv_data)
        #print(conn.recv(bufsize))
        msg_split(recv_data)
        time.sleep(10)

if __name__=="__main__":
    signal(SIGPIPE,SIG_DFL)
    signal(SIGTERM, stop_handler)
    signal(SIGINT, stop_handler)
    main()
